#!/bin/env python3

import festival
import wave
import struct
from rich import print
import os

def create_silence_file(duration=2, filename="silence.wav"):
    with wave.open(filename, "wb") as f:
        framerate=32000
        num_samples = duration * 1000 * (framerate/ 1000.0)
        f.setparams((1, 2, framerate, int(num_samples), "NONE", "not compressed"))
        # nchannels=1, sampwidth=2, framerate=32000, nframes=int(num_samples), comptype='NONE', compname='not compressed'
        for x in range(int(num_samples)):
            f.writeframes(struct.pack('h', 0))

data= []

def process_file(infile):
    with wave.open(infile, 'rb') as w:
        #print(w.getparams())
        data.append( [w.getparams(), w.readframes(w.getnframes())] )


sentences=[
        "silence_1", 
        "Hi there!", 
        "silence_1", 
        "Create a text file called 'sentences.txt! and place lines of test in there",
        "silence_1",
        "you can create delays by using silence underscore number, for delay in seconds",
        "silence_1",
        "Good luck!",
        ]

if os.path.exists("sentences.txt"):
    with open("sentences.txt") as s:
        sentences=s.readlines()

def create_silence(file):
    if not os.path.exists(file):
        duration=file.split("_",1)[1:][0]
        print(f"creating silence: {duration} for {file}")
        create_silence_file(int(duration), file)
    process_file(file)


def create_voice(text="hello"):
    print("Generating:", text)
    out=festival.textToWavFile(text)
    process_file(out)

for sentence in sentences:
    sentence=sentence.rstrip()
    if "silence_" in sentence.lower():
        print("silence detected")
        create_silence(sentence)
        continue
    create_voice(sentence)

with wave.open("generated_output.wav", "wb") as out:
    out.setparams(data[0][0])
    for i in range(len(data)):
        out.writeframes(data[i][1])


