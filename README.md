# AudioFileGenerator

This is an audio file generator written in python, using Festival TTS. It can be used to define sentences of text and delays and generate a single output wav.


## Usage

Define your text `sentences.txt` file. You can define quiet delays by using a line `silence_1` where `1` is the delay in second.

`./generate_audio.py`

will generate file `generated_output.wav`


## Installation

Install festival, including dev:

`sudo apt install festival festvox-us-slt-hts festival-dev`

Install python dependencies:

`pip install -r requirements.txt`

You will likely have to fix broken festival import in your lib/pythonX.Y/site-packages/festival.py . Simply remove the first five lines and move the `import _festival` to the left:

```diff
1c1,6
< import _festival
---
> try:
>     # module import
>     from . import _festival
> except (SystemError, ValueError):
>     # local import
>     import _festival
```
